# Sun.py - Sunrise / sunset routines

I converted a set of routines which calculate the sunrise/set times from Java to Python. Original by Paul Schlyter, written in C.
Miguel Tremblay kindly contributed an updated version, which also can calculate the sun altitude in degrees and maximal solar flux for given day in certain latitude. He also added char-encoding for newer Python versions. Thanks Miguel!
